

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'GET'
})


.then((response) => response.json())


// 4.Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
.then((titleOnly) => {
	const array1 = Object.values(titleOnly);
	const titleArray = array1.map(function(item){
		return item.title
	})
	console.log(titleArray)
})



 // 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
 fetch('https://jsonplaceholder.typicode.com/todos/14', {
 	method: 'GET'
 })


 .then((response) => response.json())

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

.then((activityNum6) => {
	console.log(activityNum6.title)
	console.log(activityNum6.completed)
})

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body :JSON.stringify({

		title: 'Created Post',
		body: 'Hello Once Again',
		userId:1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/14', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body :JSON.stringify({

		title: 'Updated Post',
		description: 'This post was recently updated.',
		status: 'Completed',
		dateCompleted:'10/26/21',
		userId:1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

 // 9. Update a to do list item by changing the data structure to contain the following properties:
 //      a. Title
 //      b. Description
 //      c. Status
 //      d. Date Completed
 //      e. User ID

 // 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

 fetch('https://jsonplaceholder.typicode.com/posts/15', {
 	method: 'PATCH',
 	headers: {
 		'Content-Type' : 'application/json'
 	},
 	body :JSON.stringify({


 		title: 'Updated Again Post',
 		description: 'This post was already updated again.',
 		status: 'Completed',
 		dateCompleted:'10/27/21',
 		userId:1


 	})
 })
 .then((response) => response.json())
 .then((json) => console.log(json))










